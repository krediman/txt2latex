from typing import Tuple, Union, List, Dict, Collection
import re
import sys
from lib.Heuristik.Heuristik import Heuristik
from lib.texttype.texttype import texttype
import config.config as config

class SongLaTexttype(texttype):
    '''Subklasse von texttype, die zusätzlich textblöcke erzeugt, die an jinja2 übergeben werden können'''

    def __init__(self, data: List[List[str]], gew_typ=None):
        texttype.__init__(self, data, gew_typ)
        self.blocktyp = ''
        self.text = []

    # override
    def _generateWorkingData(self):
        self.blocktyp = ''
        self.text = []
        return super()._generateWorkingData()

    __doc__ = texttype.__doc__ + '''
        Speichert die Daten, die hinterher in latex ausgegeben werden.
        typ: alle typen, die leadsheets kennt. z.B. verse, verse*, chorus, info

        text: wird 1:1 in den latex code übernommen. '''

    def autoTyp(self) -> int:
        '''findet automatisch den typ des texttype-objektes
        gibt die nummer der Zeile zurück, in der der Hinweis gefunden wurde
        Es wird angenommen, dass das ganze objekt nur einen einzelnen Block enthält.
        gibt die zeilennummer zurück, in der das label, falls vorhanden, steht,
        sonst -1'''
        self._updateWD()
        for i in range(len(self.str)):
            line = self.str[i]
            if re.match(config.REFRAINREGEX, line, re.IGNORECASE) is not None:
                # Auf Chorus-Wiederholungs-hinweis (Ref. ohne weiteren Text) prüfen
                if (re.sub(config.REFRAINREGEX, '', line, flags=re.IGNORECASE).replace(' ', '') == '' and len(self.str) == 1):
                    self.blocktyp = 'refrainWdh'
                else:
                    self.blocktyp = 'refrain'
                return i

            elif re.match(config.STROPHENREGEX, line, re.IGNORECASE) is not None:
                self.blocktyp = 'strophe'
                return i

            elif re.match(config.INFOREGEX, line, re.IGNORECASE) is not None:
                self.blocktyp = 'info'
                return i

            else:
                self.blocktyp = 'strophe*'
                # kein return, vielleicht findet man das label in der nächsten zeile
        return -1

    @staticmethod
    def akkordstil(akkord: str, stil: str) -> str:
        '''Konvertiert den Akkordstil (Umwandlung von Mollschreibweisen)
        stil: 'l', 'm', oder ''
        'm': e -> Em
        'l': Em -> e
        sonst: keine konvertierung'''

        if stil not in {'l', 'm'}:
            return akkord

        akkorde = akkord.split('/')  # für Doppelakkorde, etc.
        erg = []
        for akk in akkorde:
            fund = re.search(
                r'^(\(?)([a-h])([#b]?)(m?)((?:maj|add|dim|min|aug|sus)?)(\d*)(\)?)$', akk, flags=re.IGNORECASE)
            if fund is None:
                print('WARNung: Akkord "' + akkord + '" kann nicht konvertiert werden.', file=sys.stderr)
                erg.append(akk)
                continue

            g = list(fund.groups())
            # g[0] Klammer auf oder leer
            # g[1] Grundton
            # g[2] Halbton (# oder b)
            # g[3] 'm' falls Mollakkord in m-Schreibweise, sonst leer
            # g[4] Modifizierer (z.B. sus, dim) oder leer
            # g[5] ggf zusätzlicher ton, z.B. 7, 9 oder 11
            # g[6] klammer am ende des Akkordes (oder leer)
            if g[3].lower() == 'm':  # Mollakkord in m-Schreibweise: umschreiben
                if stil == 'l':
                    g[1] = g[1].lower() # Grundton klein schreiben
                    g[3] = '' # m entfernen
            elif g[1].islower():  # Akkord ist kleingeschrieben, also Moll in l-schreibweise
                if stil == 'm':
                    g[1] = g[1].upper()
                    g[3] = 'm'
            if g[2] == "b":
                g[2] = "&"
            # Alle anderen Fälle sind Dur-Akkorde. Da muss nichts umgeschrieben werden.
            erg.append(''.join(g))
        return '/'.join(erg)

    @staticmethod
    def abstandKonvertieren(zeile: str) -> str:
        # Wenn in der Zeile mehrere Leerzeichen hintereinander vorkommen,
        # bilde den leerraum mittels \hspace in Latex ab.

        # Wir wollen die Abstände zwischen den Akkorden einigermaßen abbilden:
        # ' ' -> leerzeichen
        # sonst 3 Leerzeichen -> 1em  (konfigurierbar)

        lastEnd = 0  # position des vorherigen endes
        erg = ''  # Ausgabe
        for match in re.finditer(r' {2,}', zeile, flags=re.IGNORECASE):
            beg, end = match.start(), match.end()
            erg += zeile[lastEnd: beg]
            # 3 Leerzeichen entsprechen ca. 1 em
            erg += r'\hspace{'+str('%1.4f' % ((end - beg)*config.laenge_leerzeichen)) + config.latex_laengen_einheit + '}'
            # Der ursprüngliche Text wird ab dem ende des Leerraumes weiter übernommen
            lastEnd = end  # Hier fängt das nächste Textstück an
        # Letztes Textstück nicht vergessen:
        erg += zeile[lastEnd:]
        return erg

    @staticmethod
    def akkordeInZeile(text: List[str], gew_typ: List[str], stil: str):
        '''setzt Akkord- und Textzeilen zusammen, wenn möglich.
        sonst werden zeilen rein aus akkorden generiert
        Damit das funktioniert, muss der richtige typ gewählt sein.'''

        def ATzeile(akkzeile: str, textzeile: str, stil='') -> str:
            '''baut Akkord- und Textzeile zu einer LaTeX-kompatiblen Akkordtextzeile zusammen'''

            def ATakkord(akkord: str, stil='') -> str:
                '''gibt den latex befehl zurück, der den akkord an die passende stelle in den text setzt'''
                if akkord in config.nicht_konvertierende_Akkorde:
                    return akkord
                else:
                    return r'\[' + SongLaTexttype.akkordstil(akkord, stil) + ']'

            # ersetzungen auflisten und nach Anfangsposition absteigend sortieren
            akkErs = sorted(SongLaTexttype.ersetzungen(akkzeile, 'Akkordzeile', config.Ersetzungen_pre), key=lambda ers: ers[1], reverse=True)
            txtErs = sorted(SongLaTexttype.ersetzungen(textzeile, 'Textzeile', config.Ersetzungen_pre), key=lambda ers: ers[1], reverse=True)
            akkzeile = akkzeile.ljust(len(textzeile))
            # Die Akkordzeile kann durch setzten von leerzeichen außerhalb von akkorden beliebig verlängert werden.
            # Bei der Textzeile muss ein anders zeichen gewählt werden, da es hinterher wieder entfernt werden muss.
            # verlängere die akkordzeile ggf

            for beg, end, ers in txtErs:
                # Fall 1: Über dem zu ersetzenden teil sind nur leerzeichen in der Akkordzeile
                if akkzeile[beg:end].isspace() or len(akkzeile[beg:end])==0:
                    textzeile = textzeile[:beg] + ers          + textzeile[end:]
                    akkzeile = akkzeile[:beg]   + ' '*len(ers) + akkzeile[end:]
                #Fall 2: über dem zu ersetzenden Teil befindet sich text (z.B. ein akkord)
                else:
                    # Falls der text kürzer wird: fehlende Zeichen durch ein seltenes Sonderzeichen ersetzen, das hinterher wieder löschen
                    if len(ers) < end-beg:
                        ers = ers.ljust(end-beg, '✪')
                        textzeile = textzeile[:beg] + ers + textzeile[end:]
                    else: # Text wird länger: Akkord über dem Text durch Einfügen von leerzeichen
                        akkordstart = akkzeile[beg:end].replace(' ', '')[0]
                        print("akkzeile:", akkzeile, "\nakkordstart:", akkordstart)
                        index = akkzeile[beg:].find(' ', akkzeile[beg:end].find(akkordstart))
                        if index != -1: #falls es ein leerzeichen (und potenziell weitere Akkorde gibt):
                            akkzeile = akkzeile[:index] + ' '*(len(ers)-(end-beg)) + akkzeile[index:]
                        textzeile = textzeile[:beg] + ers + textzeile[end:]

            # entferne überschüssige leerzeichen vom Ende der akkordzeile
            akkzeile = akkzeile.rstrip()

            # Einfache Ersetzungen in der Akkordzeile:
            for beg, end, ers in akkErs:
                if len(ers) <= end-beg: # der Akkord wird kürzer: mit Leerzeichen auffüllen
                    akkzeile = akkzeile[:beg] + ers + ' ' *(end-beg-len(ers)) + akkzeile[end:]
                else:
                    # Der Akkord wird länger oder bleibt gleich
                    # Text durch Einsetzen eines seltenen Sonderzeichens verlängern
                    akkzeile = akkzeile[:beg] + ers + akkzeile[beg+len(ers):]
                    textzeile = textzeile[:end] + '✪' * (len(ers) - (end-beg)) + textzeile[end:]

            # Textzeile falls nötig verlängern, bis sie wenigstens so lang ist, wie die Akkordzeile
            textzeile = textzeile.ljust(len(akkzeile))

            atz = ''  # ergebnis: die akkordtextzeile
            textpos = 0
            # iteriere über alle Akkorde der Zeile:
            for match in re.finditer(config.AKKORDREGEX, akkzeile, flags=re.IGNORECASE):
                beg, end = match.start(), match.end()
                atz += textzeile[textpos:beg]+ATakkord(akkzeile[beg:end], stil)
                textpos = beg

            atz += textzeile[textpos:]

            atz = atz.replace('✪', '')
            return atz

        def Azeile(akkzeile: str, stil='') -> str:
            '''setzt die Akkordzeile so, dass Latex die Zeichen als Akkorde ohne Text setzt'''
            return r'\nolyrics{'+ATzeile(akkzeile, '', stil)+'}'

        def Tzeile(txtzeile: str, stil='') -> str:
            '''setzt die Akkordzeile so, dass Latex die Zeichen als Akkorde ohne Text setzt'''
            return ATzeile('', txtzeile, stil)

        def postErsetzugen(line, typ):
            for beg, end, ers in sorted(SongLaTexttype.ersetzungen(line, typ, config.Ersetzungen_post), key=lambda ers: ers[1], reverse=True):
                line = line[:beg] + ers + line[end:]
            return line

        newtext = []
        newtyp = []
        prevline = None
        prevtyp = None

        # Da immer die vorherige Zeile bearbeitet wird, füge eine Zeile hinzu. Diese wird nicht bearbeitet.
        for line, line_typ in zip(text+["DIESE ZEILE SOLLTE IN DER AUSGABE NICHT AUFTAUCHEN.",], gew_typ+[None,]):
            # Unterscheide 4 Fälle:
            if prevline is None or prevtyp is None:
                pass

            elif prevtyp == 'Akkordzeile':
                if line_typ == 'Textzeile':
                    # die beiden Zeilen werden zu einer Akkordtextzeile zusammengefügt.
                    newtext.append(postErsetzugen(ATzeile(prevline, line, stil), 'AkkordTextZeile'))
                    newtyp.append('AkkordTextZeile')
                    # jetzt sind beide zeilen benutzt worden.
                    line = None
                    line_typ = None

                else:
                    # die vorherige Zeile wird als (einzelne) Akkordzeile formatiert
                    newtext.append(postErsetzugen(Azeile(prevline, stil), prevtyp))
                    newtyp.append(prevtyp)

            elif prevtyp == 'Textzeile':
                newtext.append(postErsetzugen(Tzeile(prevline, stil), prevtyp))
                newtyp.append(prevtyp)

            # Die vorherige Zeile ist eine andere Zeile (infozeile, leerzeile, Überschrift) Das sollte nicht vorkommen
            else:
                print("WARNung: Zeile \n" + prevline + "\n als", prevtyp, "erkannt. Das ist nicht sinnvoll.")
                newtext.append(postErsetzugen(prevline, prevtyp))
                newtyp.append(prevtyp)

            prevline = line
            prevtyp = line_typ

        return newtext, newtyp


    @staticmethod
    def ersetzungen(zeile: str, typ: str, confErsetzungen:Dict) -> Collection[Tuple[int, int, str]]:
        '''erstellt eine Liste mit Ersetzungen aus regex-eingabe und der dazugehörigen ersetzung
        Die ausgabe ist die liste aller ersetzungen.
        Sie bestehen aus Start, Ende und ersetzung.
        Die ersetzungen dürfen sich nicht überlappen'''
        erg = [] #liste aller Änderungen
        if typ in confErsetzungen.keys():
            ersetzungen = confErsetzungen[typ]
        elif typ is not None:
            ersetzungen = dict()
        for pattern, repl in ersetzungen.items():
            for fund in re.finditer(pattern, zeile):
                if callable(repl):
                    erg.append((fund.start(), fund.end(), repl(zeile[fund.start(): fund.end()])))
                else:
                    erg.append((fund.start(), fund.end(), repl))
        return erg

    def erstelleLatexDaten(self):
        '''erstellt den Text, der in das Latex-dokument eingefügt wird.
        es wird angenommen, dass das ganze objekt nur einen einzelenn block enthält'''
        # Zeilen automatich einrücken: regex vom anfang der zeile mit nummer linenr entfernen,
        # ohne die realive position der zeichen zur zeile darüber zu ändern.

        def labelEntfernen(self, zeilenNr, regex):
            """ Entfernt eine Markierung z.B. für eine strophe. Wenn in der Zeile darüber Akkorde stehen,
            wird sie angepasst, um die entfernte Markierung auszugleichen."""

            akt = re.sub(regex, '', self.text[zeilenNr], flags=re.IGNORECASE) # finde das label und entfernt es.
            if zeilenNr > 0:
                # Die vorherige Zeile muss ebenfalls gekürzt werden, da die sonst passen die beiden nicht mehr aufeiander
                l = len(self.text[zeilenNr])-len(akt)
                vorg = self.text[zeilenNr - 1]

                # wenn möglich, kürze die vorherige Zeile:
                if (vorg[0] == ' '): n_spaces = len(re.findall(' +', vorg)[0])
                else:                n_spaces = 0
                self.text[zeilenNr - 1] = vorg[min(l, n_spaces):]

                # verlängere ggf die aktuelle zeile
                if l > n_spaces:
                    akt = ' ' * (l - n_spaces)
            self.text[zeilenNr] = akt

        self._updateWD()
        self.text = self.str + []  # echte kopie, statt referenz
        lineNr = self.autoTyp()  # XXX: Rückgabewert sollte 0 (erste Zeile), 1 (erste Textzeile) oder -1 (kein Label) sein.

        # Labels aus dem text entfernen, falls vorhanden.
        if self.blocktyp == 'strophe*' or lineNr == -1:  # Kein Label gefunden -> nichts zu tun
            pass
        elif self.blocktyp == 'strophe':
            labelEntfernen(self, lineNr, config.STROPHENREGEX)
        elif self.blocktyp in {'refrain', 'refrainWdh'}:
            labelEntfernen(self, lineNr, config.REFRAINREGEX)
        elif self.blocktyp == 'info':
            labelEntfernen(self, lineNr, config.INFOREGEX)

        # Latex Befehle für den Anfang und das Ende der Umgebung, in die der Block geschrieben wird, setzen.
        self.umgBeg, self.umgEnd = config.Umgebungen.get(
            self.blocktyp, config.Umgebungen['standart'])

        # Formatierung vorher:
        self.text = config.preFormat(self.text, self.blocktyp)

        # Akkorde und text in eine Zeile zusammensetzen, wenn möglich und Akkorde zu erwarten sind.
        if self.blocktyp in {'strophe*', 'strophe', 'refrain'}:
            # ersetzugnen In Textzeilen und Akkordzeilen passieren in dieser funktion
            self.text, self.gew_typ = SongLaTexttype.akkordeInZeile(self.text, self.gew_typ, stil=config.Akkordstil)
            for i in range(len(self.text)):
                self.text[i] = SongLaTexttype.abstandKonvertieren(self.text[i])


        # HACK: Der Inhalt für repchorus (der nicht existiert) soll keine zusätzliche Leerzeile verursachen
        elif self.blocktyp == "repchorus":
            self.text = []

        self.text = config.postFormat(self.text, self.blocktyp)
        return
