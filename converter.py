#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Mon Mar  9 22:27:10 2020

@author: Paul Steuernagel
'''

from typing import Collection, Set, Union
from song_converter import SongKonverter
import sys
import os
import typing
import argparse
import traceback

# typing: Pfadspezifikation:
pfad = typing.Union[str, os.DirEntry]

insuffixes = {'.txt', '.lied'}
outsuffix = '.tex'
templatePfad = 'Template.jinja'


def get_dir_content(directory:pfad) -> Set[os.DirEntry]:
    # Gibt den Inhalt eines Verzeichnisses zurück
    with os.scandir(directory) as listing:
        return set(listing)


def get_files(dir_content:Set[pfad])-> Set[pfad]:
    #Gibt alle Pfade zurück, die Dateien entsprechen.
    return set(elem for elem in dir_content if elem.is_file)


def get_accessable(dir_content:Set[pfad], access=os.R_OK) -> Set[pfad]:
    # Gibt diejenigen Pfade zurück, auf die lesend / schreibend / ausführend (abhängig von access)
    #    zugegriffen werden kann.
    return set(elem for elem in dir_content if os.access(elem, access))


def filter_suffix(dir_content:Set[pfad], suffix:Union[str, Collection[str]]):
    # Gibt alle Dateien in dir_content zurück, die auf das bzw. eines der Suffix/e endet.
    erg = set()
    if type(suffix) == str:
        suffix = set((suffix, ))
    for file in dir_content:
        for suf in suffix:
            if file.name.endswith(suf):
                erg.add(file)
                break
    return erg

def is_newer(file1:pfad, file2:pfad):
    # gibt True, wenn die letzte Änderung von file1 jüngeren Datums ist als die letzte Änderung von file2
    #Die Zeit wird in sekunden sein dem Anfang der Zeit(UNIX) angegeben. eine größere Zahl entspricht einer neueren Datei
    time1 = os.path.getmtime(file1)
    time2 = os.path.getmtime(file2)
    return time1 > time2


def get_inaccessable(dir_content:Set[pfad], access=os.R_OK) -> Set[pfad]:
    # Gibt alle dateien zurück, auf die nicht lesend [schreibend/ausführend] (abhängig von access)
    #     zugegriffen werden kann.
    return set.difference(dir_content, get_accessable(dir_content, access))


def get_outfilename(infilename:str, outending:str, inendings:Collection[str]) -> str:
    '''bestimmt einen Dateinamen für die Ausgabe
    outending ist die gewünschte endung der ausgabe, Endungen in inending werden entfernt'''
    for end in inendings: # Dateiendung kürzen
        if infilename.endswith(end):
            infilename = infilename[:len(infilename)-len(end)]
            break
    return infilename + outending #Dateiname für die Ausgabe


def readfile(filename:pfad, mode='r') -> str:
    # Liest den gesamten inhalt der Datei
    chunksize = 10000 # Anzahl der Zeichen, die auf einmal gelesen werden.
    data = ''
    with open(filename, mode) as file:
        while 1:
            newdata = file.read(chunksize)
            data += newdata
            if len(newdata) < chunksize: # wurde das Ende der Datei erreicht?
                break
    return data


def writefile(filename:pfad, data:str, mode='w')->int:
    with open(filename, mode) as file:
        chars_written = file.write(data)
        if chars_written != len(data):
            return 1
    return 0


def build_path(directory:pfad, filename:str)-> pfad:
    return os.path.join(directory, filename)


def convertFile(infile:pfad, outfile: pfad, moreOutput:bool)-> None:
        # Datei laden
        if moreOutput: print(infile.name.rjust(30), ' lesen… ', end='')
        else: print('   '+infile.name)
        indata = readfile(infile)
        # Datei Konvertieren
        if moreOutput: print(' umwandeln… ', end='')
        outdata = konverter.konvertiere(indata)  # multithreading nötig/möglich?
        # Datei speichern
        if moreOutput: print(' speichern… ', end='')
        writefile(outfile, outdata)
        if moreOutput: print('fertig')


def getInfiles(directory:pfad) -> Set[pfad]:
    return get_accessable(filter_suffix(get_files(get_dir_content(directory)), insuffixes), os.R_OK)


def fileIsWriteable(datei:pfad, allow_overwrite:str, referenz:pfad =None) -> bool:
    # datei kann geschrieben werden und (es darf überschrieben werden oder die datei existiert nicht.)
    # allow_overwrite erkenne die Werte "all", "newer" und "older". Für "newer" und "older" ist eine Referenzdatei nötig.
    if os.path.exists(datei):
        # Der gegebene Pfad existiert. prüfe, ob es sich um reine Datei handelt
        # und ob sie geschriebn werden darf.
        if os.path.isfile(datei) and os.access(datei, os.W_OK):
            if allow_overwrite == "all":
                return True #Der einfache Fall
            #keine Referenz angegeben, aber eine relative Prüfung gefordert -> verbiete das überschreiben
            if referenz is None:
                return False
            # erlaube
            elif allow_overwrite == "newer" and is_newer(datei, referenz):
                return True
            elif allow_overwrite == "older" and is_newer(referenz, datei):
                return True
            else: return False
        else:
            return False  # es handelt sich um ein Verzeichnis oder wir dürfen nicht überschreiben(das heißt nicht, dass es nicht möglich wäre)
    else:
        # Datei existiert nicht. Überprüfe Schreibrechte im Ordner
        pdir = os.path.dirname(datei)
        if not pdir:
            pdir = '.'
        # Die Datei kann erzeugt werden, falls in das Verzeichnis geschrieben werde kann.
        return os.access(pdir, os.W_OK)

def rewriteFilename(dateiname:str) -> str:
    # Beschränkt den Dateinamen auf die Zeichen A-Z a-z 0-9 .-_+ (ohne Leerzeichen)
    # alle anderen Zeichen werden gelöscht.
    # Falls das erste Zeichen nach einem gelöschten Zeichen ein kleinbuchstabe ist,
    # wird er zu einem GROSSBUCHSTABEN umgewandelt.
    # Falls das erste Zeichen ein kleinbuchstabe ist, wird er zu einem GROSSBUCHSTABEN
    # umgewandelt

    def konvertiereZeichen(zeichen:str, gross=False) -> str:
        # gibt das Zeichen zurück, falls das es in der Gruppe A-Z a-z 0-9 .-_+ ist.
        # konvertiert äöüß, alle anderen zeichen werden gelöscht
        # bestimme die nummer im unicode-zeichensatz
        # Mit gross=True werden kleinbuchstaben zu großbuchstaben konvertiert.

        if zeichen.isdigit(): # 0-9
            return zeichen

        #konvertieren Umlaute
        if zeichen=='ä':
            zeichen = 'a'
        if zeichen=='Ä':
            zeichen = 'A'
        if zeichen=='ö':
            zeichen = 'o'
        if zeichen=='Ö':
            zeichen = 'O'
        if zeichen=='ü':
            zeichen = 'u'
        if zeichen=='Ü':
            zeichen = 'U'
        if zeichen=='ß':
            zeichen = 's'

        nr = ord(zeichen)
        if nr >= 65 and nr <= 90: #Großbuchstabe
            return zeichen
        if nr >= 97 and nr <= 122: #kleinbuchstabe
            if gross:
                return zeichen.upper()
            return zeichen
        if zeichen == "_": #Latex mag keine Unterstriche in Dateinamen
            return '-'
        if zeichen in ".-+":
            return zeichen
        return ''

    neuerName = ''
    naechsterGross=True
    for buchstabe in dateiname:
        neuerBuchstabe = konvertiereZeichen(buchstabe, naechsterGross)
        if neuerBuchstabe != '':
            naechsterGross = False
        else:
            naechsterGross = True
        neuerName += neuerBuchstabe
    return neuerName



if __name__== '__main__':
    # Aufrufparameter lesen
    argParser = argparse.ArgumentParser(description='''konvertiert Lieder im Textformat in LaTeX songs Dateien. Konvertiert werden alle Dateien im Eingabeverzeichnis, die auf '.txt' oder '.lied' enden. Die konvertierten Dateien werden im Ausgabeverzeichnis als '.tex'-Dateien abgespeichert.
''')
    argParser.add_argument("Eingabeverzeichnis", help="Das Verzeichnis, das die zu konvertierenden Dateien enthält.")
    argParser.add_argument("Ausgabeverzeichnis", help="Das Verzeichnis, in dem die konvertierten Dateien gespeichert werden sollen.")

    argParser.add_argument("-a", "--all_files", help="Verarbeite alle Dateien im Eingabeverzeichnis unabhängig von der Dateiendung", action="store_true")
    argParser.add_argument("-r", "--rewrite", help="Dateinamen umschreiben: entfernt alle Leer- und Sonderzeichen aus dem Dateinamen und schreibt den ersten Buchstaben jedes Wortes des Dateinamens groß.", action="store_true")

    exclGroup1 = argParser.add_mutually_exclusive_group()
    exclGroup1.add_argument('-o', '--overwrite_older', help="Überschreibe vorhandene Dateien im Zielverzeichnis, wenn die jeweilige Quelldatei neuer ist als die Zieldatei (gemessen am Änderungsdatum)", action="store_true")
    exclGroup1.add_argument('-O', '--overwrite', help="Überschreibe vorhandene Dateien im Zielverzeichnis (unabhängig vom Änderungsdatum)", action="store_true")

    exclGroup2 = argParser.add_mutually_exclusive_group()
    exclGroup2.add_argument("-v", "--verbose", help="mehr Ausgabe", action="store_true")
    exclGroup2.add_argument("-q", "--quiet", help="weniger ausgabe; nur Fehlermeldungen werden ausgegeben", action="store_true")

    args = argParser.parse_args()

    if args.all_files:
        # jede Datei soll konvertiert werden
        insuffixes.add('')

    if not (os.path.isdir(args.Eingabeverzeichnis)):
        print('Fehler: Das Eingabeverzeichnis "'+args.Eingabeverzeichnis+'" scheint nicht zu existieren.', file=sys.stderr)
        exit(1)
    if not (os.path.isdir(args.Ausgabeverzeichnis)):
        print('Fehler: Das Ausgabeverzeichnis "'+args.Ausgabeverzeichnis+'" scheint nicht zu existieren.', file=sys.stderr)
        exit(1)

    # Dateien, die gelesen werden können
    infiles = getInfiles(args.Eingabeverzeichnis)

    # Konverter laden:
    konverter = SongKonverter(templatePfad=templatePfad)

    # Jetzt konvertieren wir die einzelnen Dateien:
    # zunächst wird der Dateiname und der Pfad für die Ausgabe festgelegt.
    for infile in infiles:
        outfilename = get_outfilename(infile.name, outsuffix, insuffixes) # Dateiname für die Ausgabe
        # Dateiname ggf umschreiben
        if args.rewrite:
            outfilename = rewriteFilename(outfilename)

        outpath = build_path(args.Ausgabeverzeichnis, outfilename)     # Ausgabepfad

        # Prüfen, ob Ausgabedatei geschrieben werden kann / darf.
        if args.overwrite_older: overwrite="older"
        elif args.overwrite: overwrite="all"
        else: overwrite="no"

        if not fileIsWriteable(outpath, overwrite, referenz=infile):
            if not args.quiet:
                print(outfilename, ' darf nicht überschrieben werden. ', infile.name, ' wird übersprungen.', file=sys.stdout)
            continue # Datei überspringen

        try:
            convertFile(infile, outpath, args.verbose)
        except Exception:
            if not args.quiet:
                print('FEHLER bei Datei', infile.name, ' ', file=sys.stderr)
                if args.verbose:
                    traceback.print_tb(file=sys.stderr)
