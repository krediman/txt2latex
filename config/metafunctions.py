import re
from config.config import KATEGORIEREGEX
# Die Werte von metaschlüsseln müssen ggf. bearbeitet werden. dafür stehen hier konfigurierbare funktionen zur verfügung.abs

# Metadaten bestehen aus einem Schlüssel und einem Wert. Üblicherweise wird der WErt des Schlüssels ohne verarbeitung übernommen. 
# Falls die Werte berabeitet werden sollen, kann im dictionary metafunktions ein eintrag <Schlüssel>=<funktion> hinzugefügt werden.
# <Schlüssel> bezeichnet hierbei den schlüssel, der in der ausgabe auftaucht.
# <funktion> ist die funktion, die mit dem Wert aufgerufen werden soll.
#   <funktion> muss genau ein Argument nehmen, hier wird der Wert übegeben, wie er eingegeben wurde.
#   Die Funktion muss ein str-Objekt oder None zurückgeben. Wenn None zurückgegeben wird, wird der SChlüssel nicht ausgegeben.
#   Ansonsten wird der Rückgabewertt als Wert verwendet.

def kategorienliste(instr):
    ktgr = re.findall(KATEGORIEREGEX, instr)
    return ','.join(ktgr)

metafunctions = dict(
    ktgr=kategorienliste
)