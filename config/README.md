# Hinweise zur Konfiguration

Diese Dokument enthält die Dokumentation der Konfigurationsdateien.

## config.py

Hier werden alle wichtigen Konfigurationen durchgeführt. Der Einfachheit halber handelt es sich um ein Python-skript, sodass beliebige funktionien direkt programmiert werden können.

##### Akkordstil
Erlaubte Werte: `'l'`, `'m'` oder `''`
Akkorde können in zwei SChreibweisen Angegben werden:
 * Großkleinschreibung: Dur-Akkorde werden groß geschrieben, Moll-Akkorde werden klein geschrieben
 * 'm'-Schreibvweise: Dur-Akkorde werden groß geschrieben, Moll-Akkorde erhalten ein 'm'

Beide Schreibweise werden unterstützt. Mit dem Parameter `Akkordstil` wird festgelegt, welcher Stil in der ausgabe versendet werden soll.
`Akkordstil = ''` übernimmt die Akkorde so, wie sie in der Eingabedatei angegeben werden.
`Akkordstil = 'm'` konvertiert  die Akkorde in die m-Schreibweise, falls nötig.
`Akkordstil = 'l'` konvertiert  die Akkorde in Großkleinschreibweise, falls nötig.

#### REGEX
Um Label für unterschiedliche Blocktypen zu erkennen, werden Reguläre ausdrücke verwendet. Da die Eingabe intern Zeilenweise verarbeitet wird, werden die regulären Ausdrücke auf jede Zeile angewendet. REGEX für mehrzeiligen Text ist also nicht möglich.
Der gefundene Ausdruck wird aus der Eingabe entfernt. Es sollte nur eine Übereinstimmung in einer Zeile geben.
`STROPHENREGEX` ist der reguläre Ausdruck für die Strophennummer von Nummerierten Strophen
`REFRAINREGEX` ist der reguläre Ausdruck für die Markierung des Refrains
`INFOREGEX` ist der reguläre Ausdruck, mit dem der Wnfang des Infoblockes identifiziert wird.

`AKKOORDREGEX` wird auf Zeilen angewendet, die nur Akkorde enthalten. Jeder Treffer sollte ein einzelner Akkord sein.

#### Ersetzungsfunktionen
Vor und nach der Konvertierung der Zeilen mit Akkorden und Text zu einer gemeinsamen ausgabezeile besteht die möglichkeit, manuell änderungen vorzunehmen. Das kann abhängig vom erkannten typ des Blockes, aus dem die Zeilen Stammen in den entsprechenden Funktionen passieren. Üblicherweise werden Hier zeichen, die Latex nicht verarbeiten kann, z.B. das kaufmännische und (`&`)  durch escape-sequenzen ersetzt, im Prinzip sind aber beliebige ersetzungen möglich.

Da im Titelblock und im Infoblock keine Akkordsymbole verarbeitet werden, gibt es nur eine Ersetzungsfunktion.

Eine Besonderheit stellt die Funktion `formatMeta` dar. Hier können die aus der Eingabe extrahierten Metadaten als dict bearbeitet werden. Es empfiehlt sich, nur die Werte zu bearbeiten.

Alle Funktionen, deren Name mit einem Unterstrich beginnt, werden nur lokal verwendet.

#### Umgebungen:
Hier werden die Namen der verwendeten Latex-Umgebungen für die unterschiedlichen Blocktypen definiert. Siehe auch Template.jinja, bzw. die dazugehörige Dokumentation.
