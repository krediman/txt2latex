# Konfiguration: Benötigt von lib/texttype/SongLaTexttype
from typing import List, Dict
# 'l': Mollakkorde in Kleinbuchstaben (Am -> a),
# 'm': Mollakkorde mit m (e-> Em),
# (Leer): Keine Änderung
Akkordstil = 'l'

# mehrere Leerzeichen hintereinander werden in der Eingabe als Leerraum verwendet.
# in der ausgabe wird \hspace{…} gesetzt.
# Wenn mehrere leerzeichen gefunden werden, wird die länge entsprechend ersetzt:
# Standard ist: ein leerzeichen entspricht 1/3 em. das kann iher belieig geändert werden.
laenge_leerzeichen = 1/3
latex_laengen_einheit = 'em'

# Reguläre Ausdrücke, mit denen die verschiedenene Teile der Lieder identifiziert werden können-
STROPHENREGEX = r'^\s?\d+([).:]|( :))*\s*' # Nummern für die Strophen
REFRAINREGEX = r'^\s?Ref(rain)?[).: ]+\s*' # Bezeichnung des Refrains
INFOREGEX = r'^\s?@?info((:\s*)|\s+)'      # Bezeichnung des Infoblockes
KATEGORIEREGEX = r'\w+' # Regex, der auf kategorienamen aber nicht auf trennzeichen passt.
AKKORDREGEX = r'\S+' # muss einfach nur alles fressen, was möglicherweise ein Akkord sein könnte.

#regulärer Ausdruck für ZEilen, die nur Akkorde enthalten (spezifisch, soll nicht auf normalen Text passen)
akkord_zeilen_regex = r'( *([:|]+|(\(?([A-Ha-h](#|b)?(sus|dim|add|m(aj)?)?\d*)(\/([A-Ha-h](#|b)?(sus|dim|add|maj)?\d*))*\)?)|\^))+ *'
#regulärer Ausdruck für einen Akkord (spezifisch, soll nicht auf normalen Text passen)
akkord_regex = r'(\(?([A-Ha-h](#|b)?(sus|dim|add|m(aj)?)?\d*)(\/([A-Ha-h](#|b)?(sus|dim|add|maj)?\d*))*\)?)'

#WDHLREGEX = r'[/|]{1,2}\:' #Im Moment nicht Verwendet
#WDHRREGEX = r'\:[/|]{1,2}' #Im Moment nicht Verwendet

# Liste von Akkordzeichen, die ohne ohne Konvertierung übernommen werden.
nicht_konvertierende_Akkorde = ['^', u"𝄇", u"𝄆"]

# grobe Funktionsweise des Programmes:
# Teile mit * sind in der konfiguration komplett änderbar.
#
#      Datei lesen
#           V
#        parser
#           V
#    Formatierungen_pre*
#           V
#     Ersetzungen_pre*
#           V
#      Konvertierung
#           V
#     Ersetzungen_post*
#           V
#    Formatierungen_post*
#           V
#     Datei schreiben
#

# Ersetzungen, die in den Zeilen passieren sollen, bevor text und akkorde zusammengesetzt werden.
# Hier werden Symbole aus mehreren Zeichen zusammengesetzt, damit ssie nicht durch das zusammenfügen von Text und Akkorden kaputt gehen. Nach dem zusammensetzen findet die rückersetzung statt.
_ersetzungen_pre_rep = {
    r":[|\\/]{1,2}"  : u"𝄇",   # :|
    r"[|\\/]{1,2}:"  : u" 𝄆",   # |:
}

# Ersetzungen, die in den Zeilen passieren sollen, nachdem text und Akkorde zusammengesetzt wurden. Verwendung wie re.sub(pattern, repl). Angabe: pattern: sub#
_ersetzungen_post_rep = {
    u"𝄇" : r'\rrep ',
    u"𝄆" : r'\lrep ',
}

_UmlErsetzungen_post = {
    #"ä":r'{\"a}',
    #"ö":r'{\"o}',
    #"ü":r'{\"u}',
    #'Ä':r'{\"A}',
    #'Ö':r'{\"O}',
    #'Ü':r'{\"U}',
}

# Ersetzungen für Sonderzeichen
_ZeichenErsetzungen_pre = {
    "&": "⅋",
    "´":"'",
    "’":"'", #typographische Apostrophe und accents
    "`":"'",
}

_ZeichenErsetzungen_post = {
    "⅋":r"\&",
    "\"":r"{\dq}",
}

Ersetzungen_pre = {
    "Textzeile": dict(**_ZeichenErsetzungen_pre, **_ersetzungen_pre_rep), # & wird in Akkordzeilen für die Verminderung benutzt. die Rückersetzung findet nach dem zusammensetzen statt.
    "Akkordzeile": _ersetzungen_pre_rep,
}

Ersetzungen_post = {
    "Textzeile":        dict(**_ZeichenErsetzungen_post, **_ersetzungen_post_rep, **_UmlErsetzungen_post),
    "Akkordzeile":      dict(**_ZeichenErsetzungen_post, **_ersetzungen_post_rep, **_UmlErsetzungen_post),
    "AkkordTextZeile":  dict(**_ZeichenErsetzungen_post, **_ersetzungen_post_rep, **_UmlErsetzungen_post),
}

# Formatierungen: bevor und nachdem die Akkorde mit dem Text zusammengesetzt werden,
# können Formatierungen am Text vorgenommen werden.
# Beim Zusammensetzen werden Text und Akkorde in eine Zeile geschrieben.
# Änderungen, die an den Akkordzeilen geschehen, sollten also vorher gemacht werden.
# Eingabe und Ausgabe sind Listen, die einen String für jede Zeile enthalten.

# Ersetzungen für Umlaute
_UmlErsetzungen_pre = {}

def _Ersetzen(line:str, ersetzungen:Dict[str,str]) -> str:
    """ führt die Ersetzungen in ersetzungen auf den string line aus."""
    for orig, new in ersetzungen.items():
        line = line.replace(orig, new)
    return line

# Formatierungen Am Infotext; hier werden keine Akkorde gesetzt, daher gibt es nur eine Funktion.
def _formatInfo(text: List[str]) -> List[str]:
    for i in range(len(text)):
        text[i] = _Ersetzen(text[i], _UmlErsetzungen_pre)
        text[i] = _Ersetzen(text[i], _ZeichenErsetzungen_pre)
        text[i] = _Ersetzen(text[i], _UmlErsetzungen_post)
        text[i] = _Ersetzen(text[i], _ZeichenErsetzungen_post)
    return text

# Formatierungen Titel und Liedanfang
def formatTitel(text: List[str]) -> List[str]:
    for i in range(len(text)):
        text[i] = _Ersetzen(text[i], dict(**_UmlErsetzungen_pre, **_ZeichenErsetzungen_pre))
        text[i] = _Ersetzen(text[i], dict(**_UmlErsetzungen_post, **_ZeichenErsetzungen_post))
    return text

# Formatierungen anderen Textbasiertzen Metadaten(z.B. Autor, Album, etc.)
def formatMeta(meta: Dict[str,str]) -> Dict[str,str]:
    """erlaubt die Umformatierung der Metadaten.
    Aktuell werden nur Sonderzeichn durch latex-kompatible ersetzt. natürlich ist hier noch mehr möglich.
    meta ist ein Dictionary, dass paarre vom typ schlüssel:wert enthält, die in der Eingabe erkannt wurden"""
    for metakey in meta.keys():
        metaval = meta[metakey]
        metaval = _Ersetzen(metaval, _ZeichenErsetzungen_pre)
        metaval = _Ersetzen(metaval, _UmlErsetzungen_pre)
        metaval = _Ersetzen(metaval, _ZeichenErsetzungen_post)
        metaval = _Ersetzen(metaval, _UmlErsetzungen_post)
        meta[metakey] = metaval
    return meta

# Definiert die start- und endkommandos für die verwendeten latex-Umgebungen
Umgebungen = {
    'standart': (r'\beginverse*',      r'\endverse'),# wird verwendet, falls aus irgendeinem Grund kein anderer passt.
    'strophe*':   (r'\beginverse*',      r'\endverse'),
    'strophe':    (r'\beginverse',       r'\endverse'),
    'refrain':   (r'\beginchorus',      r'\endchorus'),
    'info':     (r'\beginscripture{}', r'\endscripture'),
    # HACK: Die Wiederholung des Refrain nutzt keine Umgebung. Erzeugt wird eine Umgebung ohne Inhalt und endstring. Das ist ein einzelnes Kommando.
    'refrainWdh': (r'\printchorus',      r''),
}


# Ohne funktion:
# Formnatierungen vor der Konvertierung der zeilen:
def preFormat(text: List[str], typ:str):
    # typ enthält den erkannten blocktyp ('strophe*', 'strophe', 'refrain', 'refrainWdh' oder 'info')
    return text

# Formnatierungen vor der Konvertierung der zeilen:
def postFormat(text: List[str], typ: str):
    # typ enthält den erkannten blocktyp ('strophe*', 'strophe', 'refrain', 'refrainWdh' oder 'info')
    if typ=='info':
        return _formatInfo(text)
    else:
        return text
