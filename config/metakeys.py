# Schlüssel für metaangaben, die im Titelblock eines Liedes Vorkommen dürfen. siehe auch die Definition in lib/Heuristik/Heuristik.py

# In den Eingabedateien soll es mehrere Möglichkeiten geben, einen Metaschlüssel zu schreiben, z.B: kann für den Autor des Liedtextes txt, text oder worte benutzt werden.
# In der Ausgabe wird ein fester Metaschlüssel verwendet. Diese zuuornung passiwert im dictionary metakeys. neue Schlüssel können hinzugefügt werden, 
# indem unten neue einträge hinzugefügt werden.
#
# SChreibweise:
# <schlüssel in der Eingabedatei>=<Schlüssel in der Ausgabedatei>
# <schlüssel in der Eingabedatei> muss hier klein geschrieben werden. in der Eingabedatei wird zwischen groß- und kleinschreibung bei Schlüsseln nicht unterschieden.


metakeys = dict(  # Siehe auch liste in Heuristik.py
    it='ititle', #titel für den Index
    ititle='ititle',
    ititel='ititle',
    indextitel='ititle',
    indextitle='ititle',
    ww='wuw',  # Worte und weise
    wuw='wuw',
    jahr='jahr',  # jahr des Liedes
    j='jahr',
    mel='mel',  # Autor der Melodie
    melodie='mel',
    weise='mel',
    melj='meljahr',  # Jahr der Melodie
    meljahr='meljahr',
    weisej='meljahr',
    weisejahr='meljahr',
    txt='txt',  # Autor des Textes
    text='txt',
    worte='txt',
    txtj='txtjahr',  # Jahr des Textes
    textj='txtjahr',
    txtjahr='txtjahr',
    textjahr='txtjahr',
    wortejahr='txtjahr',
    wortej='txtjahr',
    alb='alb',  # Album, auf dem das Lied erschienen ist.
    album='alb',
    lager='lager',  # Lager, auf dem / für das das Lied geschrieben
    tonart='tonart',  # originaltonart
    key='tonart',
    capo='capo',    # vorschlag für das setzen des Capos
    cap='capo',     # TODO: in Latex setzen
    cr="cr",
    copy="cr",
    copyright="cr",
    license="li", #lizenz
    lic="li",
    li="li",
    lizens="li",
    liz="li",
    ktgr='ktgr', # Kategorien, zu denen das Lied gehört
    kat='ktgr',
    cat='ktgr',
    kategorie='ktgr',
    bo='bo',  # Seite im Bock
    bock='bo',
    bb='bb',    #Seite im Doppelbock
    dbock='bb',
    doppelbock='bb',
    db='bb',
    cp='cp',    # Seite iom Codex Pathomomosis
    codex='cp',
    pf1='pfi',  # Seite im Pfadiralala1
    pfi='pfi',
    pf='pfi',
    pf2='pfii',  # Seite im Pfadiralala2
    pfii='pfii',
    pf3='pfiii',  # Seite im Pfadiralala3
    pfiii='pfiii',
    pf4='pfiv',
    pfiiii='pfiv',
    pfiv='pfiv',
    pf4p='pfivp',
    pfiiiip='pfivp',
    pfivp='pfivp',
    ju='ju',  # Seite in der Jurtenburg
    jurten='ju',
    jurtenburg='ju',
    gruen='gruen',  # Seite Im Grünen (Liederbuch)
    grün='gruen',
    gruenes='gruen',
    grünes='gruen',
    kss4='kssiv',  # Seite in Kinder-Schoko-Songs 4
    kssiv='kssiv',
    kssiiii='kssiv',
    siru='siru',  # Seite in Die singende Runde
    biest='biest',  # Seite im Biest
    bst='biest',
    bstn='biestchen',  #Seite im Biestchen
    biestchen='biestchen',
    tb='tb',    # Seite in der Tarmina Burgundi
    tarmina='tb',
    eg='eg',  # Seite im evangelischen Gesangbuch
    evg='eg',
    egp='egplus',  # Seite Im evangelischen Gesangbuch plus
    evgp='egplus',
    egplus='egplus',
    evgplus='egplus',
    tf='turm',    # Der Turmfalke (Landesliederbuch VCP-RPS)
    turm='turm',
    turmfalke='turm',
    tsp='tonspur',  # Die Tonspur
    ton='tonspur',
    tonspur='tonspur',
    gn='gnom',  # Der Liedergnom (betagtes Gauliederbuch Gau Nassau Oranien)
    gnom='gnom',
    liedergnom='gnom',
    gb='gnorke',    # Gnorkenbüdel
    gnorke='gnorke',
    gnorken='gnorke',
    gnorkenbüdel='gnorke',
    vq='vq',     # Seite im kleinen Vasquaner, erste version
    vq1='vq',
    vasqua='vq',
    vasquaner='vq',
    büdel="buedel", # Seite im Notenbuedel
    buedel="buedel",
    noten="buedel",
    notenbüdel="buedel",
    notenbuedel="buedel",
    nb="buedel",
    sss="singsangsingasong", #Seite in Sing Sang Sing a Song (Stammesliederbuch HVP)
    singsangsing="singsangsingasong",
    singsangsingasong="singsangsingasong",
    flp='fuellhornfleppe', #Die füllhornfleppe (nie gesehen, aber was solls. die Daten sind da, also kann ich sie auch eintragen.)
    fleppe='fuellhornfleppe',
    fuellhornfleppe='fuellhornfleppe',
    füllhornfleppe='fuellhornfleppe',)

metakeys.update
({
    "(c)":"cr", #weitere schreibweise für copyright, die anders nicht aufgeschrieben werden kann.
})
